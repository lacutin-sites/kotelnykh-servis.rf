<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use GuzzleHttp\Psr7\Uri;
use http\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" type="image/png" href="/files/24hours.png"/>
    <link rel="icon" href="/files/favikon-32x32.png" sizes="32x32" />
    <link rel="icon" href="/files/favikon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/files/favikon-180x180.png" />
    <meta name="yandex-verification" content="f6926226d2e9db0b" />
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
<!-- Preloader -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- /Preloader -->

<!-- Header Area Start -->
<header class="header-area">
    <!-- Top Header Area Start -->
    <div class="top-header-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-5">
                    <div class="top-header-content">
                        <p>Добро пожаловать в сервис котельных!</p>
                    </div>
                </div>
                <div class="col-7">
                    <div class="top-header-content text-right">
                        <p><i class="fa fa-clock-o" aria-hidden="true"></i> Мы работаем <span class="rounded">24/7</span> <span class="mx-2"></span> | <span class="mx-2"></span> <i class="fa fa-phone" aria-hidden="true"></i>  8 800 500-13-32</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Header Area End -->

    <!-- Main Header Start -->
    <div class="main-header-area">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="akameNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="/"><img src="/theme/logo.png" alt=""></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">
                        <!-- Menu Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>
                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                                <li class="<?=$this->context->route=='site/index'? 'active':'';?>" >
                                    <a href="/">Сервис Котельных</a></li>
                                <li class="<?=$this->context->route=='site/about'? 'active':'';?>">
                                    <a href="<?=\yii\helpers\Url::to(['site/about'])?>">О компании</a></li>
                                <li class="<?=$this->context->route=='site/contact'? 'active':'';?>">
                                    <a  href="<?=\yii\helpers\Url::to(['site/contact'])?>">Контакты</a></li>

                                <li style="font-size: 12px"><a href="#" >Статьи</a>
                                    <ul class="dropdown dropdown-menu-sm-left">
                                        <li  title="подробнее о сервисном (техническом)обслуживании КВАРЦ Сервис">
                                            <a href="<?=\yii\helpers\Url::to(['site/page','id'=>1])?>">
                                                о тех. обслуживании
                                            </a>
                                        </li>
                                            <li title="Эксплуатация котельных по договору аренды">
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>2])?>">
                                                    Эксплуатация
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>3])?>">
                                                    обслуживание
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>4])?>">
                                                    ГРУ и ГРПШ
                                                </a>
                                            </li>
                                            <li title="Электрохимическая защита подземных газопроводов">
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>5])?>">
                                                    Эл.хим защита
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>6])?>">
                                                    Наладка
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?=\yii\helpers\Url::to(['site/page','id'=>7])?>">
                                                    Промывка
                                                </a>
                                            </li>

                                        </li>


                                    </ul>

                                </li>
                                <li class="<?=$this->context->route=='site/news'? 'active':'';?>">
                                    <a  href="<?=\yii\helpers\Url::to(['site/news'])?>">Новости</a>
                                </li>

                            </ul>

                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
        <!--
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        -->
        <?= $content ?>


<?php $this->endBody() ?>
<div class="container">
    <div class="border-top"></div>
    <div class="row pt-5">
        <div class="col-12">
            <div class="section-heading text-center">
                <h2 class="text-uppercase">Связаться с нами</h2>
            </div>
        </div>
    </div>
</div>

<div class="akame-google-maps-area">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2287.879204783976!2d82.9451169153503!3d55.010282456479324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x42dfe61286c1f7d9%3A0x22bd0bf42a71aecb!2z0YPQuy4g0JPRg9GA0YzQtdCy0YHQutCw0Y8sIDM30JAsIDIwMSwg0J3QvtCy0L7RgdC40LHQuNGA0YHQuiwg0J3QvtCy0L7RgdC40LHQuNGA0YHQutCw0Y8g0L7QsdC7LiwgNjMwMTAy!5e0!3m2!1sru!2sru!4v1568534838894!5m2!1sru!2sru"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<!-- Contact Information Area Start -->
<section class="contact-information-area section-padding-80-0">
    <div class="container">
        <div class="row">
            <!-- Single Contact Information -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-contact-information mb-80">
                    <i class="icon_phone"></i>
                    <h4>Телефон</h4>
                    <p>8 800 500-13-32</p>
                </div>
            </div>


            <!-- Single Contact Information -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-contact-information mb-80">
                    <i class="icon_mail"></i>
                    <h4>Почта</h4>
                    <p>office@kwarts-service.ru</p>
                </div>
            </div>
            <!-- Single Contact Information -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-contact-information mb-80">
                    <i class="fa fa-instagram"></i>
                    <h4>Инстаграм</h4>
                    <p><a href="https://www.instagram.com/kwartsservice/?r=nametag" target="_blank">@kwartsservice</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Information Area End -->

<!-- Contact Area Start -->
<section class="akame-contact-area bg-gray section-padding-80">
    <div class="container">
        <div class="row">
            <!-- Section Heading -->
            <div class="col-12">
                <div class="section-heading text-center">
                    <h2 class="text-uppercase" >Мы всегда на связи</h2>
                    <p>Если у вас возникли вопросы, отправьте нам сообщения и мы объязательно с вами свяжемся</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <!-- Form -->
                <form action="#" method="post" onsubmit="sendMsg();return false;" class="akame-contact-form border-0 p-0">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" required name="name" class="form-control mb-30" placeholder="Имя">
                        </div>
                        <div class="col-lg-6">
                            <input type="email" name="email" class="form-control mb-30" placeholder="Почта">
                        </div>
                        <div class="col-lg-12">
                            <input type="tel" required name="phone" class="form-control mb-30" placeholder="Телефон">
                        </div>
                        <div class="col-12">
                            <textarea name="msg"  required class="form-control mb-30" placeholder="Сообщения"></textarea>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn akame-btn btn-3 mt-15 active">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Contact Area End -->
<!-- Footer Area Start -->
<footer class="footer-area section-padding-80-0">
    <div class="container">
        <div class="row justify-content-center align-items-center">

            <!-- Single Footer Widget -->
            <div class="col-12 col-sm-6 col-md-4">
                <div class="single-footer-widget mb-80">
                    <a href="/" class="footer-logo"><img src="files/img/core-img/logo.png" alt=""></a>
                    <div class="copywrite-text text-center">
                        <p> <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Все права защищены &copy;<script>document.write(new Date().getFullYear());</script>  <a class="hidden" href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>


        </div>
    </div>
</footer>
<script>
    function sendMsg() {
        $.post( "<?=\yii\helpers\Url::to(['site/send'])?>",{name:$('#name').val(),phone:$('#phone').val(),email:$('#email').val(),msg:$('msg').val()}, function( data ) {
            alert(data);
        });
    }
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55588870, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/55588870" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script src="//code.jivosite.com/widget.js" data-jv-id="O3TixnqOGg" async></script>
</body>
</html>
<?php $this->endPage() ?>
