<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Новости';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>
                Техническое (сервисное) обслуживание паровых котельных

            </h1>

            <div class="row ">
                <div class="col-md-4">
                    <img src="/files/img/parovoe.jpg" class="img-fluid">
                </div>
                <div class="col-md-8">
                        <p>
                            Инженеры ООО «КВАРЦ Сервис», успешно обслуживают паровые котельные, различной паропроизводительности.</p><p>
                            К нам часто обращаются новые заказчики с проблемой не выхода котла на заданные заводские параметры парообразования при работе на 80% от номинальной мощности котлов, при этом оборудование как правило отработало менее 3 -5 лет.</p>
                        <p>
                            При детальном изучении проблем мы как правило выявляем следующие:
                            - отстроены с ошибками горелочные устройства, в итоге котел работает со снижением мощности;
                            - загрязнение камеры сгорания котла, в результате недожега топлива и как следствие образование сажи на поверхности нагрева котла.
                            - халатное или поверхностное отношение к  водно-химическому режиму работы котельной, как следствии отложения на трубной доске котлов, если не проводить химическую промывку котлов, то появляется риск прогорания жаровых труб котла.
                            - не отстроена работа автоматики котла, в результате режим работы котельной не успевает обеспечивать нагрузки потребителя.
                        </p>
                        <p>
                            При этом сервисные компании годами эксплуатируют котельное оборудование, выставляя новые счета, своим Заказникам, за и техническое обслуживание и возникающие в результате низкого качества сервиса текущего ремонта.
                            Пока оборудование котельной новое, оно работает исправно даже в режимах запрещенных заводами изготовителями. Как правило проблемы на котельных возникают спустя 3-5 лет
                            Инженеры ООО «КВАРЦ Сервис», профессионально и качественно проведут диагностику оборудования Вашей котельной, в случае заинтересованности заключат договор на Техническое (сервисное) обслуживание котельной.
                        </p>
                        <p>
                            Приглашаем Всех заинтересованных лиц
                            С уважением Директор
                            ООО «КВАРЦ Сервис» Лакутина Наталья
                        </p>

                </div>
            </div>
        </div>
    </div>
</div>

