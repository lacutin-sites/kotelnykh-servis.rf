<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container service-content">
    <div class="row">
        <div class="col">
            <h1>Компания ООО «КВАРЦ Сервис»</h1>
            <ul>
                <li><p>
                        - это сервисная компания, активно развивающаяся в сфере профессионального технического обслуживания водогрейных и паровых котельных, а так же обслуживании технологических установок, нагрев которых происходит посредствам горелочных устройств.
                    </p>
                </li>
                <li> <p>
                        Компания ООО «КВАРЦ Сервис» работает с 2017 года, и за это время заработала репутацию, надежного партнера, способного профессионально выполнять техническое обслуживание и ремонт теплотехнического оборудования.
                    </p>
                </li>
                <li>
                    <p>
                        Сегодня мы оказываем услуги по сервису котельных и технологических установок от Хабаровска  до Тюмени, имеем положительный опыт сотрудничества с партнёрами из Казахстана.
                    </p>
                </li>
            </ul>

            <section class="akame-portfolio section-padding-0-80 clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="section-heading">
                                <h2>Среди партнеров с технологическим оборудованием, большое количество:</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row akame-portfolio-area">
                        <?php
                        $path='./files/img/partner';
                        $files1 = scandir($path);
                        $itemsText=['Асфальтных заводов, где требуется обслуживание сушильных барабанов и термомасленых колов;',
                            'Хлебозаводов применяемых кроме пара печи для выпечки хлебобулочных изделий;',
                            'Нефтеперерабатывающие заводы, обслуживание печей нагрева нефти;',
                            'Кирпичные заводы, обслуживание тоннельных печей сушки кирпича;',
                            'Заводы по производству сухих строительных смесей, обслуживание сушильных барабанов;',
'Элеваторы, сушка зерна и другие.'];
                        $i=0;
                        foreach ($files1 as $item){
                            if ($item!='.' &&$item!='..'){?>


                        <!-- Single Portfolio Item -->
                        <div class="col-12 col-sm-6 col-lg-4 akame-portfolio-item haircuts mb-30 wow fadeInUp" data-wow-delay="200ms">
                            <div class="akame-portfolio-single-item">
                                <img src="/files/img/partner/<?=$item?>">
                                <div class="overlay-content d-flex align-items-center justify-content-center">
                                    <div class="overlay-text text-center">
                                        <p><?=$itemsText[$i]?></p>
                                    </div>
                                </div>
                                <a href="/files/img/partner/<?=$item?>" class="thumbnail-zoom"><i class="icon_search"></i></a>
                            </div>
                        </div>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col">
                            Компания КВАРЦ Сервис является сервисным партнеров большинства производителей теплотехнического оборудования, поставляемого в Российскую Федерацию.

                        </div>
                    </div>
                </div>
            </section>
            <section class="akame-portfolio section-padding-0-80 clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="section-heading">
                                <h2>Мы являемся сервисными партнерами компаний:</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row akame-portfolio-area">
                        <?php
                        $path='./files/img/servispartner';
                        $files1 = scandir($path);
                        foreach ($files1 as $item){
                            if ($item!='.' &&$item!='..'){?>
                                <div class="col-12 col-sm-6 col-lg-4 akame-portfolio-item haircuts mb-30 wow fadeInUp" data-wow-delay="200ms">
                                    <div class="akame-portfolio-single-item">
                                        <img src="/files/img/servispartner/<?=$item?>">
                                        <div class="overlay-content d-flex align-items-center justify-content-center">
                                            <div class="overlay-text text-center">
                                            </div>
                                        </div>
                                        <a href="/files/img/partner/<?=$item?>" class="thumbnail-zoom"><i class="icon_search"></i></a>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </section>
    <h2>Директор компании ООО «КВАРЦ Сервис»</h2>
    <strong>Лакутина Наталья Павловна.</strong>
        <p >
            <h2>Наша миссия</h2> – выполнять профессиональный сервис котельного оборудования, продляя срок его службы и обеспечивая энергоэффективный режим работы.
        </p>
        </div>
        </div>
    </div>

