<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>
                ООО «Кварц Сервис»</h1>
            <p>
                ОГРН 1175476092725
                <br> ИНН 5405007677
                <br> КПП 540501001
                <br>
            </p>
            <p>
                <strong>Банковские реквизиты:</strong>
                <br> Название банка: ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ"
                <br> Город: МОСКВА
                <br> БИК: 044525999
                <br> Корр. счет: 30101810845250000999
                <br> Расчетный счет №40702810512500001876
                <br>
            </p>

            <p>
                <strong>Головной офис</strong>
                <br>
                <section class="contact-information-area ">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row text-left">
                                    <!-- Single Contact Information -->
                                    <div class="col-12 col-sm-6 col-lg-12 p-4">
                                        <div class="row single-contact-information text-left">
                                            <i class="icon_pin"></i>
                                            <div class="col-6">
                                                <h4>Адрес</h4>
            <p>630102 г. Новосибирск, ул. Гурьевская 37а оф. 201
            </p>
        </div>
    </div>
</div>
<!-- Single Contact Information -->
<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="row single-contact-information text-left">
        <i class="icon_phone"></i>
        <div class="col-6">
            <h4>Телефон</h4>
            <p>8 800 500-13-32</p>
        </div>
    </div>
</div>

<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="row single-contact-information text-left">
        <i class="icon_clock"></i>
        <div class="col-6">
            <h4>Работаем</h4>
            <p>24/7</p>
        </div>
    </div>
</div>

<!-- Single Contact Information -->
<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="single-contact-information row text-left ">
        <i class="icon_mail"></i>
        <div class="col-6">
            <h4>Почта</h4>
            <p>office@kwarts-service.ru</p>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-md-6">
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-12">
            <iframe class="col" height="400px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2287.879204783976!2d82.9451169153503!3d55.010282456479324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x42dfe61286c1f7d9%3A0x22bd0bf42a71aecb!2z0YPQuy4g0JPRg9GA0YzQtdCy0YHQutCw0Y8sIDM30JAsIDIwMSwg0J3QvtCy0L7RgdC40LHQuNGA0YHQuiwg0J3QvtCy0L7RgdC40LHQuNGA0YHQutCw0Y8g0L7QsdC7LiwgNjMwMTAy!5e0!3m2!1sru!2sru!4v1568534838894!5m2!1sru!2sru" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</div>

</div>
</div>
</section>
</p>

<p>
    <strong>филиалы:</strong>
    <section class="contact-information-area ">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row text-left">
                        <!-- Single Contact Information -->
                        <div class="col-12 col-sm-6 col-lg-12 p-4">
                            <div class="row single-contact-information text-left">
                                <i class="icon_pin"></i>
                                <div class="col-6">
                                    <h4>Адрес</h4>
<p>644100 г. Омск, ул. проспект академика Королева 3 оф. 507-2</p>
</div>
</div>
</div>
<!-- Single Contact Information -->
<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="row single-contact-information text-left">
        <i class="icon_phone"></i>
        <div class="col-6">
            <h4>Телефон</h4>
            <p>8 800 500-13-32</p>
        </div>
    </div>
</div>

<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="row single-contact-information text-left">
        <i class="icon_clock"></i>
        <div class="col-6">
            <h4>Работаем</h4>
            <p>24/7</p>
        </div>
    </div>
</div>

<!-- Single Contact Information -->
<div class="col-12 col-sm-6 col-lg-12 p-4">
    <div class="single-contact-information row text-left ">
        <i class="icon_mail"></i>
        <div class="col-6">
            <h4>Почта</h4>
            <p>office@kwarts-service.ru</p>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-md-6">
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-12">
            <iframe class="col" height="400px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2286.0874251958535!2d73.29455731524004!3d55.04168405407829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43ab000d8c9f5a53%3A0x734955553f33986d!2z0JDQutCw0LTQtdC80LjQutCwINCa0L7RgNC-0LvQtdCy0LAg0L_RgC3Rgi4sIDMsINC-0YQuIDUwNy0yLCDQntC80YHQuiwg0J7QvNGB0LrQsNGPINC-0LHQuy4sIDY0NDEwMA!5e0!3m2!1sru!2sru!4v1570125606930!5m2!1sru!2sru" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</div>

</div>
</div>
</section>
<br>
<section class="contact-information-area ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row text-left">
                    <!-- Single Contact Information -->
                    <div class="col-12 col-sm-6 col-lg-12 p-4">
                        <div class="row single-contact-information text-left">
                            <i class="icon_pin"></i>
                            <div class="col-6">
                                <h4>Адрес</h4>
                                <p>654007 г. Новокузнецк, ул. Кирова д.35</p>
                            </div>
                        </div>
                    </div>
                    <!-- Single Contact Information -->
                    <div class="col-12 col-sm-6 col-lg-12 p-4">
                        <div class="row single-contact-information text-left">
                            <i class="icon_phone"></i>
                            <div class="col-6">
                                <h4>Телефон</h4>
                                <p>8 800 500-13-32</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-12 p-4">
                        <div class="row single-contact-information text-left">
                            <i class="icon_clock"></i>
                            <div class="col-6">
                                <h4>Работаем</h4>
                                <p>24/7</p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Contact Information -->
                    <div class="col-12 col-sm-6 col-lg-12 p-4">
                        <div class="single-contact-information row text-left ">
                            <i class="icon_mail"></i>
                            <div class="col-6">
                                <h4>Почта</h4>
                                <p>office@kwarts-service.ru</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-12">
                        <iframe class="col" height="400px" <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.7786471730883!2d87.12757881517783!3d53.75782445151191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x42d0c11cdd9c0427%3A0xe6621946b6972473!2z0YPQuy4g0JrQuNGA0L7QstCwLCAzNSwg0J3QvtCy0L7QutGD0LfQvdC10YbQuiwg0JrQtdC80LXRgNC-0LLRgdC60LDRjyDQvtCx0LsuLCA2NTQwMDc!5e0!3m2!1sru!2sru!4v1570125635730!5m2!1sru!2sru" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

</p>
</div>
<div class="col-md-6">
    <div class="akame-google-maps-area">

    </div>
</div>
</div>
</div>
