<?php


$this->title = 'Котельных-сервис';

use yii\helpers\Url; ?>
<section class="welcome-area">
    <div class="welcome-slides owl-carousel">
        <div class="single-welcome-slide bg-img" style="background-image: url(/files/img/b1.jpg);background-size: contain;background-repeat: no-repeat;">
        </div>
        <div class="single-welcome-slide bg-img" style="background-image: url(/files/img/b2.jpg);background-size: contain;background-repeat: no-repeat">
        </div>
        <div class="single-welcome-slide bg-img" style="background-image: url(/files/img/b3.jpg);background-size: contain;background-repeat: no-repeat ">
        </div>
        <div class="single-welcome-slide bg-img" style="background-image: url(/files/img/b4.jpg);background-size: contain;background-repeat: no-repeat">
        </div>

    </div>
</section>
<section class="why-choose-us-area bg-gray section-padding-80-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading">
                    <h2 class="text-center text-uppercase">Мы предлагаем варианты сотрудничества:</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-start  pb-5">
            <div class="col-12 col-lg-6">
                <div class="section-heading">
                    <h2>Сервисное обслуживание котельных</h2>
                </div>
                <div class="choose-us-content ">
                    <ul>-
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Техническое обслуживание всех узлов котельной;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Техническое обслуживание  наружного газопровода;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Ведение технической документации;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Техподдержка котельной 24/7;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Выезд аварийной службы в течении 1 часа;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Использование склада ЗИП  компании «Кварц  Сервис»  с  последующей оплатой;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Контроль сроков поверок  приборов КИПиА;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Контроль срока поверки узла учета газа;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Обеспечение работы котельной в энергоэффективном режиме.</li>
                        <li>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col pr-0 pl-0">
                                        <img src="/files/img/person.png" width="32" height="auto">
                                    </div>
                                    <div class="col-10 pl-0">
                                        <a href="<?=Url::to(['site/page','id'=>1])?>"><strong style="color:red;text-decoration: underline;">Подробнее о сервисном (техническом)обслуживании</strong></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="section-heading">
                    <h2>Эксплуатация (аренда) котельной</h2>
                </div>
                <div class="choose-us-content ">
                    <ul>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Техническое обслуживание всех узлов котельной;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Эксплуатация  наружного газопровода;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Ведение технической  <span style='color:red'>и эксплуатационной</span> документации;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Техподдержка котельной 24/7;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Выезд аварийной службы в течении 1 часа;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Использование склада ЗИП  компании «Кварц  Сервис»  с  последующей оплатой;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Контроль сроков поверок  приборов КИПиА;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Контроль срока поверки узла учета газа;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Обеспечение работы котельной в энергоэффективном режиме;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Получение <span style='color:red'>лицензии опасного производственного объекта</span>;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Страхование объекта;</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Прохождении <span style='color:red'>проверок Ростехнадзора и прокуратуры</span>.</li>
                        <li>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col pr-0 pl-0">
                                        <img src="/files/img/person.png" width="32" height="auto">
                                    </div>
                                    <div class="col-10 pl-0">
                                        <a href="<?=Url::to(['site/page','id'=>2])?>"><strong style="color:red;    text-decoration: underline;">Подробнее о эксплуатации </strong></a>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="akame-blog-area  clearfix">
    <div class="container ">
        <div class="row">
            <!-- Section Heading -->
            <div class="col-12 pt-1">
                <div class="section-heading text-center">
                    <h2 class="text-uppercase">Наш сервис оценили</h2>
                </div>
            </div>
        </div>

        <div class="row text-center justify-content-center ">
            <?php
            for ($i=1;$i<=10;$i++) {
                ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-post-area mb-80 wow fadeInUp" data-wow-delay="200ms">
                        <div class="post-thumbnail">
                            <a ><img style="max-width: 200px;max-height: 200px" src="/files/img/o<?=$i?>.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>

<div class="container">
    <div class="border-top"></div>
</div>

<section class="akame-portfolio  bg-gray section-padding-0-80  pt-5 clearfix">
    <div class="container">
        <div class="row">
            <!-- Section Heading -->
            <div class="col-12">
                <div class="section-heading text-center">
                    <h2 class="text-uppercase">Благодарственные письма от партнеров</h2>
                </div>
            </div>
        </div>
        <div class="row akame-portfolio-area justify-content-center align-items-center">
            <?php
                for ($k=1;$k<=4;$k++) {
                    ?>
                    <div class="col-sm-6 col-xs-12 col-lg-3 akame-portfolio-item haircuts mb-30 wow fadeInUp"
                         data-wow-delay="200ms">
                        <div class="akame-portfolio-single-item">
                            <img src="/files/img/p<?=$k?>.jpg" alt="">

                            <!-- Overlay Content -->
                            <div class="overlay-content d-flex align-items-center justify-content-center">
                                <div class="overlay-text text-center">

                                </div>
                            </div>

                            <a href="/files/img/p<?=$k?>.jpg" class="thumbnail-zoom"><i class="icon_search"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>

        </div>


    </div>
</section>

