<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $this->enableCsrfValidation = false;
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionNews(){
        return $this->render('news');
    }
    public function actionPage($id){
        return $this->render('pages/page-'.$id);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionSend(){
        header('Content-Type: text/html; charset=utf-8');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');

        $user = $_POST;
        if($user)
        {
            $fio = $user['name'];
            $phone = $user['phone'];
            $email = $user['email'];
            $comment = $user['msg'];

            $to = "Moygazkotel@yandex.ru";
            $subject = "Новая заявка с сайта котельных-сервис.рф";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $result="";
            if($fio){
                $result.="<br> ФИО: ".$fio;
            }
            if($phone){
                $result.="<br>Телефон: ".$phone;
            }
            if($email){
                $result.="<br>Почта : ".$email;
            }
            if($comment){
                $result.="<br>комментарии : ".$comment;
            }

            $message = "
        <html>
        <head>
        <title>Заявка клиента!</title>
        </head>
        <body>
        <p>Заявка клиента!</p>
            ".$result."
        </body>
        </html>";
            mail($to, $subject, $message, $headers);

            die('Спасибо за заявку, скоро с вами свяжутся наши специалисты');
        }else{
            die('Что то пошло не так');
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionAbouttehinfo(){
        return $this->render('abouttehinfo');
    }
    public function actionAbouttehinfogaz(){
        return $this->render('abouttehinfogaz');
    }



    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionAboutsevice(){

        return $this->render('about');
    }
}
